package com.example.android.internalstorage_tutpoint;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    Button saveBtn,loadBtn;
    TextView readed;
    EditText ed1;

    String data;
    private String file="MyFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saveBtn=(Button)findViewById(R.id.save);
        loadBtn=findViewById(R.id.load);
        ed1=findViewById(R.id.editText);

        readed=findViewById(R.id.textView2);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data=ed1.getText().toString();
                try {
                    FileOutputStream fout=openFileOutput(file,MODE_PRIVATE);
                    fout.write(data.getBytes());
                    fout.close();
                    Toast.makeText(getBaseContext(),"file saved",Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        loadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    FileInputStream fin=openFileInput(file);
                    int c;
                    String temp="";
                    while((c=fin.read())!=-1){
                        temp=temp+Character.toString((char) c);
                    }
                    readed.setText(temp);
                    Toast.makeText(getBaseContext(),"file Read",Toast.LENGTH_LONG).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }
}
